<?php 
require_once __DIR__."/../models/meetup.php";
class MeetupController {


    function __construct(){
        require_once('models/meetup.php');
    }

    function getAll() {
        $meetup = new Meetup;
        $meetups = $meetup->displayAll();
        var_dump($meetups);
        return json_encode($meetups);
    }

        public function addMeetup(){
    
            $titre = $_POST['titre'];
            $description = $_POST['description'];
            $date = $_POST['date'];
            $location = $_POST['location'];
    
            $meetupModel = new Meetup;
            $newModel = $meetupModel->addMeetup($titre, $description, $location, $date);
    
            return json_encode($newModel);
        }
    }
<?php
/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 12/02/18
 * Time: 22:38
 */

use Pecee\SimpleRouter\SimpleRouter;
require_once 'APIMiddleware.php';
require_once 'controllers/LoginController.php';
require_once 'controllers/DefaultController.php';
require_once 'controllers/SubscriberController.php';
require_once 'controllers/meetupController.php';

// on ajoute un préfixe car le site se trouve dans mon cas à l'adresse :
// http://localhost/simplon/routeur/
$prefix = '/PROJETS/API';

SimpleRouter::group(['prefix' => $prefix], function () {
    SimpleRouter::post('/meetups', 'MeetupController@addMeetup');
    SimpleRouter::post('/login', 'LoginController@login')->name('login');
    SimpleRouter::get('/', 'DefaultController@defaultAction');
    SimpleRouter::get('/meetup', 'meetupController@getAll');
    SimpleRouter::get('/subscribers', 'SubscriberController@getAll')->addMiddleware(APIMiddleware::class);
    SimpleRouter::get('/subscriber/{id}', 'SubscriberController@getWithId')->addMiddleware(APIMiddleware::class);
});
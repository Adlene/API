<?php 

class Meetup {

        private $bdd;

    function __construct() {
        $user = 'root';
        $password = 'aniel01280';
        $host = 'localhost';
        $database = 'meetup';

        $this->bdd = new PDO("mysql:host=$host; dbname=$database; charset=utf8", $user, $password);
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function create($title, $location_fk, $description, $img, $date,$subscriber_id, $speaker_id) {
        $request = $this->bdd->prepare('INSERT INTO meetup VALUES(NULL, :title, :location_fk, :description, :img, :date, :subscriber_id, :speaker_id)');
        $request->execute(['title' => $title,
        'location_fk' => $location_fk,
        'description' => $description,
        'img' => $img,
        'date' => $date,
        'subscriber_id' => $subscriber_id,
        'speaker_id' => $speaker_id]);
    }

    public function displayOne($id){
        $request = $this->$bdd->prepare('SELECT * FROM meetup WHERE id = :id');
        $request->execute(['id' => $id]);
        $fetch = $request->fetch();
        return $fetch;
    }

    public function delete($id) {
        $request = $this->bdd->prepare('DELETE * FROM meetup WHERE id = :id');
        $request->execute(['id'=>$id]);
    }

    public function update($title, $location_fk, $description, $img, $date,$subscriber_id, $speaker_id) {
    $request = $this->$bdd->prepare('UPDATE meetup SET title=:title, location_fk=:location_fk,description=:description, img=:img, date=:date, subscriber_id=:subscriber_id, speaker_id=:speaker_id WHERE id=:id');
    $request->execute(['title' => $title,
        'location_fk' => $location_fk,
        'description' => $description,
        'img' => $img,
        'date' => $date,
        'subscriber_id' => $subscriber_id,
        'speaker_id' => $speaker_id]);
    }

    public function displayAll() {
        $request = $this->bdd->prepare('SELECT * FROM meetup');
        $request->execute();
        $fetch = $request->fetchAll(PDO::FETCH_ASSOC);
        return $fetch;
    }
    public function addMeetup($titre, $description, $location, $date) {

        $request = $this->bdd->prepare('INSERT INTO meetup (title, location, description, date)
        VALUES (:a, :b, :c, :d);');
        $request->execute([
            'a' => $titre,
            'b' => $location,
            'c' => $description,
            'd' => $date,
        ]);

        return [
            'id' => $this->bdd->lastInsertId(),
            'title' => $titre,
            'location' => $location,
            'description' => $description,
            'date' => $date,
        ];
    }

}


?>
<?php

class Speaker {

    private $bdd;

    function __construct() {
        $user = 'root';
        $password = 'qwertybitch';
        $host = 'localhost';
        $database = 'API';

        $this->bdd = new PDO("mysql:host=$host; dbname=$database; charset=utf8", $user, $password);
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    
    private $firstName;
    private $lastName;
    private $description;

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

}

?>